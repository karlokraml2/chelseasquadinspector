package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun TabButton(
    text: String,
    isActive: Boolean,
    onClick: () -> Unit,
    modifier: Modifier
) {
    val textOverflow = if (isActive) TextOverflow.Visible
    else TextOverflow.Ellipsis
    Button(
        shape = CutCornerShape(24.dp),
        elevation = null,
        colors = if (isActive) {
            ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = Color.Blue
            )
        } else {
            ButtonDefaults.buttonColors(
                contentColor = Color.Black,
                containerColor = Color.LightGray
            )
        },
        modifier = modifier.fillMaxHeight(),
        onClick = onClick
    ) {
        Text(
            text = text,
            fontSize = 11.sp,
            textAlign = TextAlign.Center,
            overflow = textOverflow,
            maxLines = 1
        )
    }
}
