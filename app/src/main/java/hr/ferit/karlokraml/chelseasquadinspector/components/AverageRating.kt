package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.ferit.karlokraml.chelseasquadinspector.data.Player
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingBlue
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingGreen
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingGrey
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingOrange
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingRed

@Composable
fun AverageRating(
    player: Player
) {
    Row(
        horizontalArrangement = Arrangement.SpaceAround,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp)
    ) {
        Text(
            text = "Average Match Rating",
            color = Color.Blue,
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
        )
        Button(
            onClick = { /*TODO*/ },
            shape = RoundedCornerShape(10.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = when (player.rating) {
                    in 1.0f..5.9f -> RatingRed
                    in 6.0f..6.4f -> RatingOrange
                    in 6.5f..6.9f -> RatingGrey
                    in 7.0f..8.9f -> RatingGreen
                    in 9.0f..10.0f -> RatingBlue
                    else -> Color.Black
                }
            ),
            modifier = Modifier
                .height(55.dp)
                .width(85.dp)
        ) {
            Text(
                text = when (player.rating) {
                    0.0f -> "N/A"
                    10.0f -> "10"
                    else -> String.format("%.1f", player.rating)
                },
                textAlign = TextAlign.Center,
                letterSpacing = 1.sp,
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 17.sp,
                maxLines = 1,
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .fillMaxWidth()
            )
        }
    }
}

