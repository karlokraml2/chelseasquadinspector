package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.ferit.karlokraml.chelseasquadinspector.data.Player
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerCategory

@Composable
fun Stats(
    player: Player
) {
    Text(
        text = "Stats",
        textDecoration = TextDecoration.Underline,
        color = Color.Blue,
        fontWeight = FontWeight.Bold,
        fontSize = 20.sp,
        modifier = Modifier.padding(top = 8.dp)
    )
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp)
            .border(2.dp, Color.Blue)
            .background(Color.Blue)
            .shadow(10.dp, spotColor = Color.Blue)
    ) {
        item {
            Spacer(modifier = Modifier.height(2.dp))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = Color.LightGray)
                    .padding(8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(text = "Appearances", fontWeight = FontWeight.Bold, color = Color.Black)
                Text(text = "${player.appearances}", color = Color.Black)
            }
            Spacer(modifier = Modifier.height(2.dp))
            if (player.category == PlayerCategory.GOALKEEPER.toString()) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.LightGray)
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "Clean Sheets", fontWeight = FontWeight.Bold, color = Color.Black)
                    Text(text = "${player.cleanSheets}", color = Color.Black)
                }
                Spacer(modifier = Modifier.height(2.dp))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.LightGray)
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "Saves", fontWeight = FontWeight.Bold, color = Color.Black)
                    Text(text = "${player.saves}", color = Color.Black)
                }
                Spacer(modifier = Modifier.height(2.dp))
            } else {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.LightGray)
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "Goals", fontWeight = FontWeight.Bold, color = Color.Black)
                    Text(text = "${player.goals}", color = Color.Black)
                }
                Spacer(modifier = Modifier.height(2.dp))
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(color = Color.LightGray)
                        .padding(8.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(text = "Assists", fontWeight = FontWeight.Bold, color = Color.Black)
                    Text(text = "${player.assists}", color = Color.Black)
                }
                Spacer(modifier = Modifier.height(2.dp))

            }
        }
    }
}