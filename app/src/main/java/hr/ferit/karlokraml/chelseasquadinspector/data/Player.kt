package hr.ferit.karlokraml.chelseasquadinspector.data

data class Player(
    var id: String = "",
    val name: String = "",
    val nationality: String = "",
    val category: String = "",
    val image: String = "",
    val number: Int = 0,
    var appearances: Int = 0,
    var goals: Int = 0,
    var assists: Int = 0,
    var cleanSheets: Int = 0,
    var saves: Int = 0,
    var rating: Float = 0.0f,
    var isFavorited: Boolean = false
)