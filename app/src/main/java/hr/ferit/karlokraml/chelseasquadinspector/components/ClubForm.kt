package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingGreen
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingOrange
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.RatingRed

@Composable
fun ClubForm() {
    val numberOfGames = 6
    val map = mapOf("W" to RatingGreen, "D" to RatingOrange, "L" to RatingRed)

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(horizontal = 20.dp)
    ) {
        Text(
            text = "Club Form:",
            color = Color.Blue,
            fontSize = 16.sp,
            textAlign = TextAlign.Center,
            fontWeight = FontWeight.SemiBold,
            modifier = Modifier.weight(1f)
        )
        LazyRow(
            contentPadding = PaddingValues(12.dp),
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .weight(1.5f)
        ) {
            items(numberOfGames) {
                FormChip(
                    text = map.keys.elementAt(it%3),
                    backgroundColor = map.values.elementAt(it%3)
                )
                Spacer(modifier = Modifier.width(4.dp))
            }
        }
    }
}