package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerCategory
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel

@Composable
fun Positions(
    navigation: NavHostController,
    viewModel: PlayerViewModel
) {
    var currentActiveButton by remember { mutableStateOf(0) }

    Column {

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = 8.dp, vertical = 12.dp)
                .background(Color.Transparent)
                .fillMaxWidth()
                .height(44.dp)
        ) {
            TabButton(
                text = "Goalkeepers",
                isActive = currentActiveButton == 0,
                onClick = { currentActiveButton = 0 },
                modifier = if (currentActiveButton == 0) {
                    Modifier.weight(1.5f)
                } else {
                    Modifier.weight(1f)
                }
            )
            TabButton(
                text = "Defenders",
                isActive = currentActiveButton == 1,
                onClick = { currentActiveButton = 1 },
                modifier = if (currentActiveButton == 1) {
                    Modifier.weight(1.5f)
                } else {
                    Modifier.weight(1f)
                }
            )
            TabButton(
                text = "Midfielders",
                isActive = currentActiveButton == 2,
                onClick = { currentActiveButton = 2 },
                modifier = if (currentActiveButton == 2) {
                    Modifier.weight(1.5f)
                } else {
                    Modifier.weight(1f)
                }
            )
            TabButton(
                text = "Forwards",
                isActive = currentActiveButton == 3,
                onClick = { currentActiveButton = 3 },
                modifier = if (currentActiveButton == 3) {
                    Modifier.weight(1.5f)
                } else {
                    Modifier.weight(1f)
                }
            )
        }

        when(currentActiveButton){
            0 -> PlayerList(selectedCategory = PlayerCategory.GOALKEEPER,viewModel, navigation)
            1 -> PlayerList(selectedCategory = PlayerCategory.DEFENDER,viewModel, navigation)
            2 -> PlayerList(selectedCategory = PlayerCategory.MIDFIELDER,viewModel, navigation)
            3 -> PlayerList(selectedCategory = PlayerCategory.FORWARD,viewModel, navigation)
        }
    }
}