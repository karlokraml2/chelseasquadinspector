package hr.ferit.karlokraml.chelseasquadinspector

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.ChelseaSquadInspectorTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        val viewModel by viewModels<PlayerViewModel>()
        setContent {
            ChelseaSquadInspectorTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavigationController(viewModel)
                }
            }
        }
    }
}



