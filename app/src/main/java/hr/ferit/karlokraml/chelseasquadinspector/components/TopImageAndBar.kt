package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberAsyncImagePainter
import hr.ferit.karlokraml.chelseasquadinspector.R
import hr.ferit.karlokraml.chelseasquadinspector.data.Player
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.LightBlue

@Composable
fun TopImageAndBar(
    player: Player,
    navigation: NavController,
    viewModel: PlayerViewModel
) {
    Column(
        modifier = Modifier
            .background(color = LightBlue)
            .fillMaxWidth()
            .height(300.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp)
                .statusBarsPadding()
                .padding(10.dp)
        ) {
            CircularButton(iconResource = R.drawable.ic_arrow_back) {
                navigation.navigateUp()
            }
            Text(
                text = "Player Details",
                color = Color.Black,
                fontWeight = FontWeight.SemiBold,
                fontSize = 20.sp,
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
            )
            CircularButton(
                iconResource = if (player.isFavorited) R.drawable.baseline_star_24
                else R.drawable.baseline_star_border_24
            ){
                player.isFavorited = !player.isFavorited
                viewModel.updatePlayer(player)
            }
        }
        Spacer(modifier = Modifier.height(5.dp))
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Blue
                        ),
                        startY = 300f
                    )
                )
        ) {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Image(
                    painter = rememberAsyncImagePainter(model = player.image),
                    contentDescription = null,
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(0.8f)
                )
                Text(
                    text = "Name: ${player.name} \n" +
                            "Nationality: ${player.nationality} \n" +
                            "Kit number: ${player.number} \n" +
                            "Position: ${player.category}",
                    fontSize = 16.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.SemiBold,
                    fontFamily = FontFamily.Monospace,
                    modifier = Modifier
                        .weight(1.2f)
                        .align(alignment = Alignment.CenterVertically)
                )
            }
        }
    }
}