package hr.ferit.karlokraml.chelseasquadinspector.data

enum class PlayerCategory {
    GOALKEEPER,
    DEFENDER,
    MIDFIELDER,
    FORWARD
}