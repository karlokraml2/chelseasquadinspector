package hr.ferit.karlokraml.chelseasquadinspector.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import hr.ferit.karlokraml.chelseasquadinspector.components.AverageRating
import hr.ferit.karlokraml.chelseasquadinspector.components.Stats
import hr.ferit.karlokraml.chelseasquadinspector.components.TopImageAndBar
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.LightBlue

@Composable
fun PlayerDetailScreen(
    navigation: NavController,
    viewModel: PlayerViewModel,
    playerId: Int
) {
    val player = viewModel.playersData[playerId]
    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .background(color = LightBlue)
    ) {
        TopImageAndBar(player = player, navigation = navigation, viewModel)
        Stats(player = player)
        AverageRating(player = player)
    }
}