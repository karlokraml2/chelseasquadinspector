package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import hr.ferit.karlokraml.chelseasquadinspector.R

@Composable
fun ScreenTitle() {
    Box(modifier = Modifier
        .height(110.dp)
        .fillMaxWidth()
        .background(color = Color.Blue)){
        Row (
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ){
            Image(
                painter = painterResource(id = R.drawable.chelsealogo),
                contentDescription = "Chelsea logo",
                contentScale = ContentScale.Crop,
                modifier = Modifier.padding(horizontal = 10.dp)
            )
            Text(
                text = "Chelsea Squad Inspector",
                modifier = Modifier
                    .padding(10.dp)
                    .fillMaxSize()
                    .align(alignment = Alignment.CenterVertically),
                fontFamily = FontFamily.Serif,
                fontWeight = FontWeight.Bold,
                fontStyle = FontStyle.Italic,
                fontSize = 28.sp,
                color = Color.White,
                textAlign = TextAlign.Center,
                letterSpacing = 2.sp,
                maxLines = 2,
                lineHeight = 34.sp
                )
        }
    }
}


