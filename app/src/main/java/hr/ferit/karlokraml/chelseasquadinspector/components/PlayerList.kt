package hr.ferit.karlokraml.chelseasquadinspector.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import hr.ferit.karlokraml.chelseasquadinspector.Routes
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerCategory
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel


@Composable
fun PlayerList(
    selectedCategory: PlayerCategory,
    viewModel: PlayerViewModel,
    navigation: NavHostController
) {
    val filteredPlayers = viewModel.playersData.filter { it.category == selectedCategory.toString() }
    LazyRow(
        contentPadding = PaddingValues(16.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .height(320.dp)
    ) {
        items(filteredPlayers.size) {
            PlayerCard(player = filteredPlayers[it]){
                navigation.navigate(
                    Routes.getPlayerDetailsPath(viewModel.playersData.indexOf(filteredPlayers[it]))
                )
            }
        }
    }
}