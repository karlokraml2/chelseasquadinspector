package hr.ferit.karlokraml.chelseasquadinspector.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import hr.ferit.karlokraml.chelseasquadinspector.R
import hr.ferit.karlokraml.chelseasquadinspector.Routes
import hr.ferit.karlokraml.chelseasquadinspector.components.ClubForm
import hr.ferit.karlokraml.chelseasquadinspector.components.IconButton
import hr.ferit.karlokraml.chelseasquadinspector.components.Positions
import hr.ferit.karlokraml.chelseasquadinspector.components.ScreenTitle
import hr.ferit.karlokraml.chelseasquadinspector.components.SearchBar
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.LightBlue

@Composable
fun MainScreen(
    navigation: NavHostController,
    viewModel: PlayerViewModel
) {
    val scrollState = rememberLazyListState()
    LazyColumn(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        state = scrollState,
        modifier = Modifier
            .fillMaxSize()
            .background(color = LightBlue)
    ) {
        item {
            ScreenTitle()
            SearchBar(
                iconResource = R.drawable.ic_search,
                labelText = "Enter player's name",
                navigation = navigation,
                viewModel = viewModel
            )
            Positions(navigation, viewModel)
            ClubForm()
            Spacer(modifier = Modifier.height(20.dp))
            IconButton(iconResource = R.drawable.baseline_star_border_24, text = "See favorites", side = 1){
                navigation.navigate(Routes.FAVORITES_SCREEN)
            }
        }
    }
}
