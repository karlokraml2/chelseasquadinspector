package hr.ferit.karlokraml.chelseasquadinspector.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import hr.ferit.karlokraml.chelseasquadinspector.R
import hr.ferit.karlokraml.chelseasquadinspector.Routes
import hr.ferit.karlokraml.chelseasquadinspector.components.CircularButton
import hr.ferit.karlokraml.chelseasquadinspector.components.PlayerCardHorizontal
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.ui.theme.LightBlue

@Composable
fun FavoritesScreen(
    navigation: NavController,
    viewModel: PlayerViewModel
) {
    val scrollState = rememberLazyListState()
    val filteredPlayers = viewModel.playersData.filter { it.isFavorited }
    Column (
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize().background(color = LightBlue)
    ){
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp)
                .statusBarsPadding()
                .padding(10.dp)
        ) {
            CircularButton(iconResource = R.drawable.ic_arrow_back) {
                navigation.navigateUp()
            }
            Text(
                text = "Favorites",
                color = Color.Black,
                fontWeight = FontWeight.SemiBold,
                fontSize = 20.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .padding(start = 100.dp)
            )
        }
        LazyColumn(
            contentPadding = PaddingValues(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(8.dp),
            state = scrollState,
            modifier = Modifier
                .fillMaxWidth()
        ) {
            items(filteredPlayers.size) {
                PlayerCardHorizontal(player = filteredPlayers[it]) {
                    navigation.navigate(
                        Routes.getPlayerDetailsPath(viewModel.playersData.indexOf(filteredPlayers[it]))
                    )
                }
            }
        }
    }

}

