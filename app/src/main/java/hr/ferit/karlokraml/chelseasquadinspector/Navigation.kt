package hr.ferit.karlokraml.chelseasquadinspector

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import hr.ferit.karlokraml.chelseasquadinspector.data.PlayerViewModel
import hr.ferit.karlokraml.chelseasquadinspector.screens.FavoritesScreen
import hr.ferit.karlokraml.chelseasquadinspector.screens.MainScreen
import hr.ferit.karlokraml.chelseasquadinspector.screens.PlayerDetailScreen

object Routes {
    const val MAIN_SCREEN = "playerList"
    const val PLAYER_DETAILS_SCREEN = "playerDetails/{playerId}"
    const val FAVORITES_SCREEN = "favoritePlayers"
    fun getPlayerDetailsPath(playerId: Int?) : String {
        if (playerId != null && playerId != -1) {
            return "playerDetails/$playerId"
        }
        return "playerDetails/0"
    }
}

@Composable
fun NavigationController(
    viewModel: PlayerViewModel
) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination =
    Routes.MAIN_SCREEN) {
        composable(Routes.MAIN_SCREEN) {
            MainScreen(
                viewModel = viewModel,
                navigation = navController
            )
        }
        composable(
            Routes.PLAYER_DETAILS_SCREEN,
            arguments = listOf(
                navArgument("playerId") {
                    type = NavType.IntType
                }
            )
        ) {backStackEntry ->
            backStackEntry.arguments?.getInt("playerId")?.let {
                PlayerDetailScreen(
                    viewModel = viewModel,
                    navigation = navController,
                    playerId = it
                )
            }
        }
        composable(Routes.FAVORITES_SCREEN){
            FavoritesScreen(navigation = navController, viewModel = viewModel)
        }
    }
}

